package de.htwg.moco.arscavengerhunt.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.factories.LocationFactory;
import de.htwg.moco.arscavengerhunt.model.impl.Location;

import static junit.framework.Assert.*;

public class LocationFactoryTest {

    public static String MISSING_EXCEPTION = "There should be an exception";

    /**
     * test what happens if we give null as parameter instead of a string list
     */
    @Test
    public void testFromStringWithNull() {

        try {
            LocationFactory.createLocationListFromStringList(null);
            fail(MISSING_EXCEPTION);
        } catch (Exception e) {
            assertTrue(e != null);
        }

    }

    @Test
    public void testFromLocationWithNull() {
        try {
            LocationFactory.createStringLocationListFromLocations(null);
            fail(MISSING_EXCEPTION);
        } catch (Exception e) {
            assertTrue(e != null);
        }
    }

    /**
     * Test static methods with empty lists
     */
    @Test
    public void testWithEmpty() {
        List<String> stringLocationListFromLocations = LocationFactory.createStringLocationListFromLocations(new ArrayList<>());
        assertTrue(stringLocationListFromLocations.isEmpty());

        List<Location> locationListFromStringList = LocationFactory.createLocationListFromStringList(new ArrayList<>());
        assertTrue(locationListFromStringList.isEmpty());
    }

    /**
     * Tests what happens with a false String
     */
    @Test
    public void testWIthFalseString() {

        String falseString = "Hallo I am not a Location!";
        List<String> falseStrings = new ArrayList<>();
        falseStrings.add(falseString);
        try {
            LocationFactory.createLocationListFromStringList(falseStrings);
            fail(MISSING_EXCEPTION);
        } catch (Exception e) {
            assertTrue(e != null);
        }
    }

    @Test
    public void testWithIncompleteInformation() {
        Location incomplete = new Location();
        incomplete.setName("I am only a name");
        List<Location> locations = new ArrayList<>();
        List<String> stringLocationListFromLocations = LocationFactory.createStringLocationListFromLocations(locations);
        assertTrue(stringLocationListFromLocations.isEmpty());
        List<String> incopmelteStinrgs = new ArrayList<>();
        incopmelteStinrgs.add(incomplete.toString());
        try{
            LocationFactory.createLocationListFromStringList(incopmelteStinrgs);
            fail(MISSING_EXCEPTION);
        }catch (Exception e){
            assertTrue(e != null);
        }

    }

    /**
     * This method tests if the two static methods work
     */
    @Test
    public void testStaticLocationMethods() {

        Location location = new Location();
        location.setLongitude(10);
        location.setLatitude(15);
        location.setAltitude(1000);
        location.setName("testy mctestFace");

        Location location2 = new Location();
        location2.setLongitude(10);
        location2.setLatitude(15);
        location2.setAltitude(1000);
        location2.setName("Some place");

        List<Location> locations = new ArrayList<>();
        locations.add(location);
        locations.add(location2);
        List<String> stringLocationListFromLocations = LocationFactory.createStringLocationListFromLocations(locations);
        //should test if there is anything
        assertFalse(stringLocationListFromLocations.isEmpty());
        //the two locations should be in there
        assertEquals(locations.size(), stringLocationListFromLocations.size());
        //the first one is testy
        assertTrue(stringLocationListFromLocations.get(0).contains("testy"));

        //form the locations back from the strings
        List<Location> locationListFromStringList = LocationFactory.createLocationListFromStringList(stringLocationListFromLocations);

        assertFalse(locationListFromStringList.isEmpty());
        assertEquals(stringLocationListFromLocations.size(), locationListFromStringList.size());
        assertTrue(locationListFromStringList.get(0).getName().equals(location.getName()));

    }

}
