package de.htwg.moco.arscavengerhunt.model;

import com.google.firebase.database.DataSnapshot;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.lang.reflect.Executable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import de.htwg.moco.arscavengerhunt.model.factories.SessionFactory;
import de.htwg.moco.arscavengerhunt.model.impl.DefaultHuntImpl;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;

import static junit.framework.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SessionFactoryTest {

    private static final String EXCEPTION_MISSING = "There should be an Exception";

    @Test
    public void createFromDataWithNull(){
        try{
            SessionFactory.createSessionFromDataSnapshot(null);
            fail(EXCEPTION_MISSING);
        }catch (Exception e){
            //this should happen
        }
    }

    @Test
    public void createFromJSONWithNull(){
        try{
            SessionFactory.createSessionFromJsonData(null);
            fail(EXCEPTION_MISSING);
        }catch (Exception e){
            //this should happen
        }
    }

    @Test
    public void createFromData(){

        DataSnapshot snapshot = mock(DataSnapshot.class);
        HashMap<String, Long> bits = new HashMap<>();
        bits.put("mostSignificantBits", 10l);
        bits.put("leastSignificantBits", 1000l);
        String name = "Muster Maxmann";
        Boolean started = false;
        Map<String, Long> playerPoints = new HashMap<>();
        Long winingPoints = 10L;
        Hunt hunt = new DefaultHuntImpl();
        hunt.setAuthor(name);
        hunt.setName(name);
        hunt.setLocations(new ArrayList<>());
        DataSnapshot returnSnapshot = mock(DataSnapshot.class);
        when(returnSnapshot.getValue()).thenReturn(bits).thenReturn(name).thenReturn(started).thenReturn(playerPoints).thenReturn(winingPoints).thenReturn(hunt);
        when(snapshot.child(anyString())).thenReturn(returnSnapshot);
        Session session = SessionFactory.createSessionFromDataSnapshot(snapshot);
        assertEquals(session.getName(), name);
    }

    @Test
    public void createFromJSON(){
        JSONObject jsonObject = mock(JSONObject.class);
        try{
            when(jsonObject.get("matchID")).thenReturn(UUID.randomUUID().toString());
            when(jsonObject.getString("name")).thenReturn("Muster Maxman");
            when(jsonObject.getBoolean("started")).thenReturn(false);
            when(jsonObject.getInt("winingPoints")).thenReturn(10);
            when(jsonObject.get("playerPoints")).thenReturn(jsonObject);
            Set<String> key = new HashSet<>();
            String player = "ha";
            key.add(player);
            when(jsonObject.keys()).thenReturn(key.iterator());
            when(jsonObject.getLong(player)).thenReturn(10l);
            when(jsonObject.get("hunt")).thenReturn(jsonObject);
            when(jsonObject.getString("author")).thenReturn("AUTHOR");
            JSONArray array = mock(JSONArray.class);
            when(array.length()).thenReturn(1);
            when(array.get(anyInt())).thenReturn(jsonObject);
            when(jsonObject.getDouble("altitude")).thenReturn(2.0);
            when(jsonObject.get("longitude")).thenReturn(20.5);
            when(jsonObject.getDouble("latitude")).thenReturn(900.6);
            when(jsonObject.getJSONArray("locations")).thenReturn(array);

            Session session = SessionFactory.createSessionFromJsonData(jsonObject);
            assertEquals("Muster Maxman", session.getName());
            assertFalse(session.isStarted());
        }catch (Exception e){
            e.printStackTrace();
            fail(e.getMessage());
        }

    }

    @Test
    public void incompleteData(){

        try{
            DataSnapshot snapshot = mock(DataSnapshot.class);
            HashMap<String, Long> bits = new HashMap<>();
            bits.put("mostSignificantBits", 10l);
            bits.put("leastSignificantBits", 1000l);
            String name = "Muster Maxmann";
            Boolean started = true;
            Map<String, Long> playerPoints = new HashMap<>();
            DataSnapshot returnSnapshot = mock(DataSnapshot.class);
            when(returnSnapshot.getValue()).thenReturn(bits).thenReturn(name).thenReturn(started).thenReturn(playerPoints);
            when(snapshot.child(anyString())).thenReturn(returnSnapshot);
            Session session = SessionFactory.createSessionFromDataSnapshot(snapshot);
            fail(session  + " should not be created");
        }catch (Exception e){
            assertTrue(e != null);
        }
    }
    @Test
    public void inCompleteJSON(){
        JSONObject jsonObject = mock(JSONObject.class);
        try{
            when(jsonObject.get("matchID")).thenReturn(UUID.randomUUID().toString());
            when(jsonObject.getString("name")).thenReturn("Muster Maxman");
            when(jsonObject.getBoolean("started")).thenReturn(false);
            when(jsonObject.getInt("winingPoints")).thenReturn(10);
            when(jsonObject.get("playerPoints")).thenReturn(jsonObject);
            Set<String> key = new HashSet<>();
            String player = "ha";
            key.add(player);
            when(jsonObject.keys()).thenReturn(key.iterator());
            when(jsonObject.getLong(player)).thenReturn(10l);
            JSONArray array = mock(JSONArray.class);
            when(array.length()).thenReturn(1);
            when(array.get(anyInt())).thenReturn(jsonObject);
            when(jsonObject.getDouble("altitude")).thenReturn(2.0);
            when(jsonObject.get("longitude")).thenReturn(20.5);
            when(jsonObject.getDouble("latitude")).thenReturn(900.6);
            when(jsonObject.getJSONArray("locations")).thenReturn(array);

            Session session = SessionFactory.createSessionFromJsonData(jsonObject);
            fail(session + " should not be created");
        }catch (Exception e){
            assertTrue(e != null);
        }
    }

}
