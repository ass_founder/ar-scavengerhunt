var World = {


    latitude: 3.0,
    longitude: 3.0,
    altitude: 3.0,
    allMarkers: [],
    allLocations: [],

    init: function initFn() {
        this.createOverlays();
    },

    createOverlays: function createOverlaysFn() {

        this.tracker = new AR.InstantTracker({
            onChangedState:  function onChangedStateFn(state) {
                // react to a change in tracking state here
            },
            // device height needs to be as accurate as possible to have an accurate scale
            // returned by the Wikitude SDK
            deviceHeight: 1.0,
            onError: function(errorMessage) {
                alert(errorMessage);
            }
        });
        
        this.instantTrackable = new AR.InstantTrackable(this.tracker, {
            drawables: {
            },
            onTrackingStarted: function onTrackingStartedFn() {
                // do something when tracking is started (recognized)

            },
            onTrackingStopped: function onTrackingStoppedFn() {
                // do something when tracking is stopped (lost)

            },
            onError: function(errorMessage) {
                alert(errorMessage);
            }
        });
    },

    changeTrackerState: function changeTrackerStateFn() {

        if (this.tracker.state === AR.InstantTrackerState.INITIALIZING) {
            
            document.getElementById("tracking-start-stop-button").src = "assets/buttons/stop.png";
            document.getElementById("tracking-height-slider-container").style.visibility = "hidden";
            
            this.tracker.state = AR.InstantTrackerState.TRACKING;
        } else {
            
            document.getElementById("tracking-start-stop-button").src = "assets/buttons/start.png";
            document.getElementById("tracking-height-slider-container").style.visibility = "visible";
            
            this.tracker.state = AR.InstantTrackerState.INITIALIZING;
        }
    },


    changeTrackingHeight: function changeTrackingHeightFn(height) {
        this.tracker.deviceHeight = parseFloat(height);
    },

    sendCurrentLocation: function sendCurrentLocation() {
        var json = {
            type:"addLocation",
            latitude: World.latitude,
            longitude: World.longitude,
            altitude: World.altitude,
            name: "testname"
        };
        AR.platform.sendJSONObject(json);
    },

    addGeoMarker: function addGeoMarker(lat,lon,alt,name) {


        // create the marker
        var markerDrawable_idle = new AR.ImageResource("assets/maps-and-flags.png");
        var markerLocation = new AR.GeoLocation(lat, lon, alt);


        var markerImageDrawable_idle = new AR.ImageDrawable(markerDrawable_idle, 2.5, {
            zOrder: 0,
            opacity: 1.0,
            onClick: function(marker) {
                var json = {
                    type:"clickLocation",
                    latitude: lat,
                    longitude: lon,
                    altitude: alt,
                    name: name
                };
                AR.platform.sendJSONObject(json);
            }
        });

        // create GeoObject
        var markerObject = new AR.GeoObject(markerLocation, {
            enabled: false,
            drawables: {
                cam: [markerImageDrawable_idle]
            }
        });
        World.allLocations.push(markerLocation);
        World.allMarkers.push(markerObject);
    }
};

World.init();

AR.context.onLocationChanged = function(lat, lon, alt, acc) {
    World.latitude = lat;
    World.longitude = lon;
    World.altitude = alt;
    World.allLocations.forEach(function(location, index) {
        var marker = World.allMarkers[index];
        var distance = measure(lat,lon,location.latitude, location.longitude);
        if(marker.enabled === false && distance < 20) {
            marker.enabled = true;
            alert("you are close to a marker");
        } else if(marker.enabled === true && distance > 20) {
            marker.enabled = false;
            alert("you left the proximity of a marker");
        }
    });
    var updateNotifier = document.getElementById("updateNotifier");
    updateNotifier.style.display = 'block';
    setTimeout(function(){
        updateNotifier.style.display = 'none';
    }, 3000);
};


//taken from https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters
function measure(lat1, lon1, lat2, lon2){  // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
    var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d * 1000; // meters
}