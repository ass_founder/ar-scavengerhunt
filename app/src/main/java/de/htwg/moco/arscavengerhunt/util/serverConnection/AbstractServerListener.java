package de.htwg.moco.arscavengerhunt.util.serverConnection;


import android.os.AsyncTask;

public abstract class AbstractServerListener<A> extends AsyncTask<String, Void, Boolean> {
    private static final String TAG = AbstractServerListener.class.getSimpleName();

    private Update<A> updateEventHandler;

    public interface Update<A> {
        void runOnUpdate(A dao);
    }

    public void setUpdateEventHandler(Update<A> eventHandler) {
        this.updateEventHandler = eventHandler;

    }

    protected void notifyAboutUpdate(A dao) {
        if (updateEventHandler != null) {
            updateEventHandler.runOnUpdate(dao);
        }
    }
}
