package de.htwg.moco.arscavengerhunt.persitance.database;

import java.util.List;

/**
 * Interface to save content or loead it.
 * @param <T> The Class of the object to load/save
 *
 */
public interface IDatabase<T> {

    /**
     * Loads every entity of the parameter T found in the database
     * @return a ArrayList of T objects
     */
    List<T> loadAllData();

    /**
     * Saves the T object in the Database
     * @param data the Object of the instance T to save
     */
    void saveData(T data);

    /**
     * Deletes the object again if its not needed
     * @param data the instance of a Class T
     */
    void deleteData(T data);
}
