package de.htwg.moco.arscavengerhunt.persitance.database;


import android.content.SharedPreferences;

import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;

/**
 * This class can be used to load the Databases.
 */
public final class DatabaseFactory {

    /**
     * Returns an IDatabse for the type Hunt.
     * @param preferences to load it from an internal storage file
     * @return the HuntDatabaseImpl in form of an IDatabase
     */
    public static IDatabase<Hunt> getHuntDatabase(SharedPreferences preferences){
        return new HuntDatabaseImpl(preferences);
    }
}
