package de.htwg.moco.arscavengerhunt.activities.lobby;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Set;
import java.util.concurrent.ExecutionException;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.dashboard.DashboardActivity;
import de.htwg.moco.arscavengerhunt.activities.game.GameActivity;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.session.SessionDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.session.SessionDaoFactory;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.user.UserDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.user.UserDaoFactory;
import de.htwg.moco.arscavengerhunt.util.constants.MiscConstants;
import de.htwg.moco.arscavengerhunt.util.serverConnection.AsyncRestCallServer;
import de.htwg.moco.arscavengerhunt.util.serverConnection.ServerConstants;

public class LobbyActivity extends AppCompatActivity {

    private static final String TAG = LobbyActivity.class.getSimpleName();
    private static final int ERROR_MSG_DELAY_MILLIS = 3000;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private UserDao userDao = UserDaoFactory.getInstance();

    private SessionDao sessionDao;
    private String sessionId;

    private Session session;

    private boolean isCreator = false;

    private TextView currentSessionName;
    private TextView creatorName;
    private TextView playerName;
    private TextView errorMsg;

    private Button startGameButton;

    private  boolean gameStarted = false;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);

        firebaseAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                Intent i = new Intent(LobbyActivity.this, LoginActivity.class);
                //close activity on backButton (new root)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        sessionId = (String) getIntent().getExtras().get(MiscConstants.KEY_SESSION_ID);

        sessionDao = SessionDaoFactory.getInstance(sessionId);

        currentSessionName = findViewById(R.id.tv_lobby_currentSession_Name);
        creatorName = findViewById(R.id.tv_lobby_creator_Name);
        playerName = findViewById(R.id.tv_lobby_player_Name);

        startGameButton = findViewById(R.id.bt_lobby_startGame);

        sessionDao.setUpdateEventHandler(innerSessionDao -> {
            session = innerSessionDao.getSession();
            if (session == null) {
                Intent i = new Intent(LobbyActivity.this, DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                toastMessage(getString(R.string.lobby_sessionClosed_message));
                killAll();
            } else {
                currentSessionName.setText(session.getName());
                isCreator = innerSessionDao.getSession().getHunt().getAuthor().equals(firebaseUser.getUid());
                StringBuilder builder = new StringBuilder();
                Set<String> players = session.getPlayers();
                if (!isCreator) {
                    playerName.setText(firebaseUser.getDisplayName());
                    String creatorNameFromDatabase = userDao.getUser(session.getHunt().getAuthor()).getName();
                    creatorName.setText(creatorNameFromDatabase);
                    players.remove(session.getHunt().getAuthor());
                    builder.append(userDao.getUser(firebaseUser.getUid()).getName());
                } else {
                    creatorName.setText(firebaseUser.getDisplayName());
                }

                for (String s : players) {
                    if (!s.equalsIgnoreCase(firebaseUser.getUid())) {
                        String playerNameFromDatabase = userDao.getUser(s).getName();
                        builder.append(playerNameFromDatabase + " ");
                    }
                }

                playerName.setText(builder.toString().trim());
                startGameButton.setVisibility(View.GONE);

                errorMsg = findViewById(R.id.tv_lobby_errorMsg);

                if (isCreator) {
                    startGameButton.setVisibility(View.VISIBLE);
                    startGameButton.setOnClickListener(startGame -> {
                        if (playerName.getText() == null || playerName.getText().toString().trim().isEmpty()) {
                            setErrorMsgWithDelay(getString(R.string.lobby_errorStart_message));
                        } else {
                            Intent intent = new Intent(LobbyActivity.this, GameActivity.class);
                            intent.putExtra(MiscConstants.KEY_SESSION_ID,sessionId);
                            startGame();
                            startActivity(intent);
                            killAll();
                        }
                    });
                }else{
                    if(session.isStarted() && !gameStarted){
                        gameStarted = true;
                        Intent intent = new Intent(LobbyActivity.this, GameActivity.class);
                        intent.putExtra(MiscConstants.KEY_SESSION_ID,sessionId);
                        startActivity(intent);
                        killAll();
                    }
                }
            }
        });

        setActionBar();

    }

    private void killAll(){
        sessionDao.removeSessionDaoListener();
        sessionDao.removeUpdateEventHandler();
        finish();
        return;
    }

    @Override
    //back navigation to last activity
    public boolean onSupportNavigateUp() {
        deleteSessionOnBackFunction();
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        deleteSessionOnBackFunction();
        Intent i = new Intent(LobbyActivity.this, DashboardActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        killAll();
    }


    private void startGame(){
        String urlParameter = String.format("?matchID=%s", sessionId);
        String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.startGame, urlParameter);
        try {
            new AsyncRestCallServer().execute(url).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void deleteSessionOnBackFunction() {
        if (isCreator) {
            String urlParameter = String.format("?sessionId=%s", sessionId);
            String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.deleteSession, urlParameter);
            try {
                new AsyncRestCallServer().execute(url).get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e(TAG, e.getMessage());
            }
        } else {
            String urlParameter = String.format("?matchID=%s&playerName=%s", sessionId, firebaseUser.getUid());
            String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.leaveSession, urlParameter);
            try {
                new AsyncRestCallServer().execute(url).get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void setErrorMsgWithDelay(String message) {
        errorMsg.setText(message);
        Handler h = new Handler();
        h.postDelayed(() -> errorMsg.setText(R.string.emptyString), ERROR_MSG_DELAY_MILLIS);
    }

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
