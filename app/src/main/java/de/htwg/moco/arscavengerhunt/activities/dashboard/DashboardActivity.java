package de.htwg.moco.arscavengerhunt.activities.dashboard;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.wikitude.architect.ArchitectView;
import com.wikitude.common.permission.PermissionManager;

import java.util.Arrays;
import java.util.List;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.about.AboutActivity;
import de.htwg.moco.arscavengerhunt.activities.hunt.CreateHuntActivity;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.activities.map_and_location.AbstractLocationUpdateProvider;
import de.htwg.moco.arscavengerhunt.activities.profile.ProfileActivity;
import de.htwg.moco.arscavengerhunt.activities.session.create_session.CreateSessionActivity;
import de.htwg.moco.arscavengerhunt.activities.session.brows_session.SessionBrowserActivity;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.location.LocationDaoFactory;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.location.LocationsDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.user.UserDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.user.UserDaoFactory;
import de.htwg.moco.arscavengerhunt.model.impl.Location;
import de.htwg.moco.arscavengerhunt.util.wikitude.PermissionUtil;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleCategory;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleData;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleJsonParser;

public class DashboardActivity extends AbstractLocationUpdateProvider {

    private Button profileButton;
    private Button createHuntButton;
    private Button createSessionButton;
    private Button sessionBrowserButton;

    private TextView currentUserName;
    private TextView currentUserEmail;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser firebaseUser;

    private List<SampleCategory> categories;
    private static final String sampleDefinitionsPath = "samples/samples.json";
    private final PermissionManager permissionManager = ArchitectView.getPermissionManager();

    private UserDao userDao = UserDaoFactory.getInstance();
    private LocationsDao locationDao = LocationDaoFactory.getInstance();

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setActionBar();

        firebaseAuth = FirebaseAuth.getInstance();

        profileButton = findViewById(R.id.bt_dashboard_profile);
        createHuntButton = findViewById(R.id.bt_dashboard_createHunt);
        createSessionButton = findViewById(R.id.bt_dashboard_createSession);
        sessionBrowserButton = findViewById(R.id.bt_dashboard_browseSession);

        currentUserName = findViewById(R.id.currentUser_Name);

        mAuthListener = firebaseAuth -> {
            if(firebaseAuth.getCurrentUser() == null){
                Intent i = new Intent(DashboardActivity.this, LoginActivity.class);
                //close activity on backButton (new root)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        userDao.setUpdateEventHandler((innerUserDao)->{
            currentUserName.setText(innerUserDao.getUser(firebaseUser.getUid()).getName());
        });

        currentUserEmail = findViewById(R.id.currentUser_Email);
        currentUserEmail.setText(firebaseUser.getEmail());

        createHuntButton.setOnClickListener(viewCreateHunt -> {
            Intent intent = new Intent(DashboardActivity.this, CreateHuntActivity.class);
            startActivity(intent);
        });

        sessionBrowserButton.setOnClickListener(viewSessionBrowser -> {
            Intent intent = new Intent(DashboardActivity.this, SessionBrowserActivity.class);
            startActivity(intent);
        });

        createSessionButton.setOnClickListener(viewCreateSession -> {
            Intent intent = new Intent(DashboardActivity.this, CreateSessionActivity.class);
            startActivity(intent);
        });

        profileButton.setOnClickListener(viewProfile ->{
            Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
            startActivity(intent);
        });

        locationDao.setUpdateEventHandler((innerDao) -> {
            List<Location> locs = innerDao.getLocations();
            for(Location loc : locs) {
                Log.d("In Dashboard",loc.toString());
            }
        });

        // For tracking Location
        mRequestingLocationUpdates = true;
        updateValuesFromBundle(savedInstanceState);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();


        final String json = SampleJsonParser.loadStringFromAssets(this, sampleDefinitionsPath);
        categories = SampleJsonParser.getCategoriesFromJsonString(json);

        final SampleData sampleData = categories.get(0).getSamples().get(0);
        final String[] permissions = PermissionUtil.getPermissionsForArFeatures(sampleData.getArFeatures());

        permissionManager.checkPermissions(DashboardActivity.this, permissions, PermissionManager.WIKITUDE_PERMISSION_REQUEST, new PermissionManager.PermissionManagerCallback() {
            @Override
            public void permissionsGranted(int requestCode) {
                //nothing
            }

            @Override
            public void permissionsDenied(@NonNull String[] deniedPermissions) {
                Toast.makeText(DashboardActivity.this, getString(R.string.permissions_denied) + Arrays.toString(deniedPermissions), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void showPermissionRationale(final int requestCode, @NonNull String[] strings) {
                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(DashboardActivity.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle(R.string.permission_rationale_title);
                alertBuilder.setMessage(getString(R.string.permission_rationale_text) + Arrays.toString(permissions));
                alertBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> permissionManager.positiveRationaleResult(requestCode, permissions));
                AlertDialog alert = alertBuilder.create();
                alert.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                Intent intent = new Intent(DashboardActivity.this, AboutActivity.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void setActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
    }
}
