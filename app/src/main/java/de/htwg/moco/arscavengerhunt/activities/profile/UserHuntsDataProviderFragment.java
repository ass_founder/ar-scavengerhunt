package de.htwg.moco.arscavengerhunt.activities.profile;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

@SuppressLint("ValidFragment")
public class UserHuntsDataProviderFragment extends Fragment {
    private AbstractDataProvider mDataProvider;
    private List<Hunt> hunts;

    public  UserHuntsDataProviderFragment(List<Hunt> hunts){
        this.hunts = hunts;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);  // keep the mDataProvider instance
        mDataProvider = new UserHuntDataProvider(hunts);
    }

    public AbstractDataProvider getDataProvider() {
        return mDataProvider;
    }
}
