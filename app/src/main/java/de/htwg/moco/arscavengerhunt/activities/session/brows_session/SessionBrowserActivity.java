package de.htwg.moco.arscavengerhunt.activities.session.brows_session;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.lobby.LobbyActivity;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.model.factories.SessionFactory;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.util.constants.MiscConstants;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;
import de.htwg.moco.arscavengerhunt.util.serverConnection.AsyncRestCallServer;
import de.htwg.moco.arscavengerhunt.util.serverConnection.ServerConstants;
import de.htwg.moco.arscavengerhunt.util.serverConnection.ServerListener;

public class SessionBrowserActivity extends AppCompatActivity {

    private static final String TAG = SessionBrowserActivity.class.getSimpleName();

    private static final int ERROR_MSG_DELAY_MILLIS = 3000;
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private ServerListener serverListener = new ServerListener();

    private Button joinSessionButton;
    private TextView errorMsg;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_browser);

        firebaseAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                Intent i = new Intent(SessionBrowserActivity.this, LoginActivity.class);
                //close activity on backButton (new root)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        // once for initial load
        setSessionDataProviderValues(savedInstanceState);

        serverListener.setUpdateEventHandler(innerListener -> {
            setSessionDataProviderValues(savedInstanceState);
        });

        serverListener.execute();

        setActionBar();

        joinSessionButton = findViewById(R.id.bt_sessionBrowser_join);
        errorMsg = findViewById(R.id.tv_sessionBrowser_errorMsg);

        joinSessionButton.setOnClickListener(join -> {
            if (!isListElementSelected()) {
                setErrorMsgWithDelay(getString(R.string.sessionBrowser_error_noSessionSelected));
            } else {
                try {
                String uid = firebaseAuth.getUid();
                String sessionId = getSelectedItem().getMatchID().toString();
                String urlParameter = String.format("?matchID=%s&playerName=%s", sessionId,uid);
                String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.joinSession, urlParameter);
                new AsyncRestCallServer().execute(url).get();
                Intent intent = new Intent(SessionBrowserActivity.this, LobbyActivity.class);
                intent.putExtra(MiscConstants.KEY_SESSION_ID, sessionId.replaceAll("\"",""));
                startActivity(intent);
                finish();
                } catch (Exception e) {
                    Log.e(TAG,e.getMessage());
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public AbstractDataProvider getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((SessionDataProviderFragment) fragment).getDataProvider();
    }

    /**
     * This method will be called when a list item is clicked
     *
     * @param position The position of the item within data set
     */
    public void onItemClicked(int position) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);

        // for selecting only one item
        List<AbstractDataProvider.Data> dataList = getDataProvider().getAllItems();
        for (AbstractDataProvider.Data data : dataList) {
            data.setSelected(false);
        }

        AbstractDataProvider.Data data = getDataProvider().getItem(position);
        data.setSelected(true);

        ((SelectSessionFragment) fragment).notifyItemChanged();

        //FOR DEBUG
        //toastMessage("Item with ID: " + data.getId() + " selected");
    }


    private List<Session> loadSessionsFromServer() {
        String url = String.format("%s/%s/%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.availableSessions);
        List<Session> sessions = new ArrayList<>();
        try {
            JSONArray sessionJson = new JSONArray(new AsyncRestCallServer().execute(url).get());
            Log.d(TAG, sessionJson.toString());

            for (int i = 0; i < sessionJson.length(); i++) {
                sessions.add(SessionFactory.createSessionFromJsonData(sessionJson.getJSONObject(i)));
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return sessions;
    }


    private void setSessionDataProviderValues(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            List<Session> sessions = loadSessionsFromServer();
            if (getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER) != null) {
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER)).commit();
                getSupportFragmentManager().beginTransaction()
                        .add(new SessionDataProviderFragment(sessions), FRAGMENT_TAG_DATA_PROVIDER)
                        .commitAllowingStateLoss();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .add(new SessionDataProviderFragment(sessions), FRAGMENT_TAG_DATA_PROVIDER)
                        .commitAllowingStateLoss();
            }

            if (getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW) != null) {
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW)).commit();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.sessionBrowser_recyclerView_container, new SelectSessionFragment(), FRAGMENT_LIST_VIEW)
                        .commitAllowingStateLoss();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.sessionBrowser_recyclerView_container, new SelectSessionFragment(), FRAGMENT_LIST_VIEW)
                        .commitAllowingStateLoss();
            }
        }
    }


    private Session getSelectedItem(){
        for (Object o : getDataProvider().getAllItems()) {
            AbstractDataProvider.Data data = (AbstractDataProvider.Data) o;
            if(data.isSelected()){
                return (Session)data.getModel();
            }
        }
        return null;

    }


    private boolean isListElementSelected() {
        boolean checkSelected = false;
        List<AbstractDataProvider.Data> dataList = getDataProvider().getAllItems();
        for (AbstractDataProvider.Data data : dataList) {
            if (data.isSelected()) {
                checkSelected = true;
            }
        }
        return checkSelected;
    }

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void setErrorMsgWithDelay(String message) {
        errorMsg.setText(message);
        Handler h = new Handler();
        h.postDelayed(() -> errorMsg.setText(R.string.emptyString), ERROR_MSG_DELAY_MILLIS);
    }

}
