package de.htwg.moco.arscavengerhunt.activities.session.create_session;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.model.interfaces.Model;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

public class HuntDataProvider  extends AbstractDataProvider<Hunt> {

    private List<HuntDataProvider.ConcreteData> mData;
    private HuntDataProvider.ConcreteData mLastRemovedData;
    private int mLastRemovedPosition = -1;

    public HuntDataProvider(List<Hunt> hunts) {
        if(hunts == null){
            mData = new LinkedList<>();
        }else{
            mData = new LinkedList<>();
            for(Hunt hunt: hunts){
                mData.add(new ConcreteData(mData.size(),0,hunt.getName(),hunt));
            }
        }

    }

    @Override
    public List<Data> getAllItems(){
        List<Data> dataList = new LinkedList<>();
        dataList.addAll(mData);
        return dataList;
    }

    @Override
    public List<Hunt> getAllModels() {
        List<Hunt> hunts = new LinkedList<>();
        for(HuntDataProvider.ConcreteData concreteData : mData){
            hunts.add((Hunt) concreteData.getModel());
        }
        return hunts;
    }

    @Override
    public int getCount() {
        return mData.size();
    }


    @Override
    public int addItem(Model model) {
        Hunt hunt = (Hunt) model;
        mData.add(new ConcreteData(mData.size(),0,hunt.getName(),hunt));

        //for notify recyclerView
        return mData.size();
    }

    @Override
    public Data getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mData.get(index);
    }


    @Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < mData.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mData.size();
            }

            mData.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final HuntDataProvider.ConcreteData item = mData.remove(fromPosition);

        mData.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    @Override
    public void swapItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        Collections.swap(mData, toPosition, fromPosition);
        mLastRemovedPosition = -1;
    }

    @Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final HuntDataProvider.ConcreteData removedItem = mData.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public static final class ConcreteData extends Data {

        private final long mId;
        private final String mText;
        private Hunt mHuntModel;
        private final int mViewType;
        private boolean mSelected = false;
        private boolean mPinned;

        ConcreteData(long id, int viewType, String text,Hunt huntModel) {
            mId = id;
            mViewType = viewType;
            mHuntModel = huntModel;
            mText = makeText(id, text);
        }

        private static String makeText(long id, String text) {
            final StringBuilder sb = new StringBuilder();

            //for DEBUG
            //sb.append(id);
            //sb.append(" - ");

            sb.append(text);

            return sb.toString();
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public int getViewType() {
            return mViewType;
        }

        @Override
        public Model getModel() {
            return mHuntModel;
        }

        @Override
        public long getId() {
            return mId;
        }

        @Override
        public String toString() {
            return mText;
        }

        @Override
        public String getText() {
            return mText;
        }

        @Override
        public boolean isSelected() {
            return mSelected;
        }

        @Override
        public void setSelected(boolean selected) {
            mSelected = selected;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }
    }
}
