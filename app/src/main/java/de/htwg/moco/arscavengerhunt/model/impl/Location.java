package de.htwg.moco.arscavengerhunt.model.impl;

import com.google.gson.Gson;
import de.htwg.moco.arscavengerhunt.model.interfaces.Model;

public class Location implements Model {

    public static final String DB_NAME = "locations";

    private String name;
    private double latitude;
    private double longitude;
    private double altitude;

    public Location() {

    }

    public static String getDbName() {
        return DB_NAME;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    @Override
    public String toString() {
        return "name:"+this.name+"latitude:" + this.latitude + " longitude:" + this.longitude + " altitude:" + this.altitude;
    }

    public String toJsonString(){
        Gson gson = new Gson();
        return  gson.toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        return name != null ? name.equals(location.name) : location.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
