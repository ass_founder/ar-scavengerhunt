package de.htwg.moco.arscavengerhunt.persitance.dao;


/**
 * An Abstract DAO to use for our DAOS.
 * @param <A> the class that should be persisted
 */
public abstract class AbstractDao<A> {

    private static final String TAG = AbstractDao.class.getSimpleName();

    private Update<A> updateEventHandler;

    public interface Update<A> {
        void runOnUpdate(A dao);
    }

    public void setUpdateEventHandler(Update<A> eventHandler) {
        this.updateEventHandler = eventHandler;

    }

    public void removeUpdateEventHandler(){
        this.updateEventHandler = null;
    }

    protected void notifyAboutUpdate(A dao) {
        if (updateEventHandler != null) {
            updateEventHandler.runOnUpdate(dao);
        }
    }

}
