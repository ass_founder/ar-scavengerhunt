package de.htwg.moco.arscavengerhunt.activities.ar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.wikitude.architect.ArchitectStartupConfiguration;
import com.wikitude.architect.ArchitectView;

import java.io.IOException;
import java.util.List;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.model.factories.LocationFactory;
import de.htwg.moco.arscavengerhunt.model.impl.Location;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.location.LocationDaoFactory;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.location.LocationsDao;
import de.htwg.moco.arscavengerhunt.util.constants.MiscConstants;
import de.htwg.moco.arscavengerhunt.util.serverConnection.AsyncRestCallServer;
import de.htwg.moco.arscavengerhunt.util.serverConnection.ServerConstants;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleData;

public class ArActivity extends AppCompatActivity {

    /**
     * Root directory of the sample AR-Experiences in the assets dir.
     */
    private static final String SAMPLES_ROOT = "samples/";

    /**
     * The path to the AR-Experience. This is usually the path to its index.html.
     */
    private String arExperience;

    private LocationsDao locationDao = LocationDaoFactory.getInstance();
    private static final String TAG = ArActivity.class.getSimpleName();

    private static final int PRIVATE_MODE_FOR_LOCAL_DB = 0;

    public static final String INTENT_EXTRAS_KEY_SAMPLE = "sampleData";

    protected ArchitectView architectView;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;

    private String sessionId;
    private List<Location> locations;
    private String userID = "";
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 3000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //reads the SessionID from intent
        sessionId = (String) getIntent().getExtras().get(MiscConstants.KEY_SESSION_ID);
        List<String> locationsAsStrings = getIntent().getStringArrayListExtra("LocationList");
        locations = LocationFactory.createLocationListFromStringList(locationsAsStrings);
        userID = (String) getIntent().getExtras().get(MiscConstants.KEY_USER_ID);


        createLocationRequest();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // Used to enabled remote debugging of the ArExperience with google chrome https://developers.google.com/web/tools/chrome-devtools/remote-debugging
        WebView.setWebContentsDebuggingEnabled(true);

        final Intent intent = getIntent();
        if (!intent.hasExtra(INTENT_EXTRAS_KEY_SAMPLE)) {
            throw new IllegalStateException(getClass().getSimpleName() +
                    " can not be created without valid SampleData as intent extra for key " + INTENT_EXTRAS_KEY_SAMPLE + ".");
        }

        final SampleData sampleData = (SampleData) intent.getSerializableExtra(INTENT_EXTRAS_KEY_SAMPLE);
        arExperience = sampleData.getPath();


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        /*
         * The ArchitectStartupConfiguration is required to call architectView.onCreate.
         * It controls the startup of the ArchitectView which includes camera settings,
         * the required device features to run the ArchitectView and the LicenseKey which
         * has to be set to enable an AR-Experience.
         */
        final ArchitectStartupConfiguration config = new ArchitectStartupConfiguration(); // Creates a config with its default values.
        config.setLicenseKey(getString(R.string.wikitude_license_key)); // Has to be set, to get a trial license key visit http://www.wikitude.com/developer/licenses.
        config.setCameraPosition(sampleData.getCameraPosition());       // The default camera is the first camera available for the system.
        config.setCameraResolution(sampleData.getCameraResolution());   // The default resolution is 640x480.
        config.setCameraFocusMode(sampleData.getCameraFocusMode());     // The default focus mode is continuous focusing.
        config.setCamera2Enabled(sampleData.isCamera2Enabled());        // The camera2 api is disabled by default (old camera api is used).
        config.setFeatures(sampleData.getArFeatures());


        architectView = new ArchitectView(this);
        architectView.onCreate(config); // create ArchitectView with configuration

        setContentView(architectView);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                android.location.Location lastLocation = locationResult.getLastLocation();
                if (lastLocation != null) {
                    architectView.setLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), lastLocation.getAltitude(), lastLocation.getAccuracy());
                }
            }
        };


        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        architectView.onPostCreate();
        try {
            /*
             * Loads the AR-Experience, it may be a relative path from assets,
             * an absolute path (file://) or a server url.
             *
             * To get notified once the AR-Experience is fully loaded,
             * an ArchitectWorldLoadedListener can be registered.
             */
            architectView.load(SAMPLES_ROOT + arExperience);
            architectView.registerWorldLoadedListener(new ArchitectView.ArchitectWorldLoadedListener() {

                @Override
                public void worldWasLoaded(String s) {
                    List<Location> allMarkers;
                    if (checkIfSessionExists()) {
                        allMarkers = locations;

                    } else {
                        //this is for the debug view
                        allMarkers = locationDao.getLocations();
                    }
                    addLocationsToAR(allMarkers);

                }

                @Override
                public void worldLoadFailed(int i, String s, String s1) {

                }
            });

            architectView.addArchitectJavaScriptInterfaceListener(jsonObject -> {
                Log.d("got json Object", jsonObject.toString());
                try {
                    if (jsonObject.getString("type").equals("addLocation")) {

                        Location loc = new Location();
                        loc.setAltitude(jsonObject.getDouble("altitude"));
                        loc.setLatitude(jsonObject.getDouble("latitude"));
                        loc.setLongitude(jsonObject.getDouble("longitude"));
                        loc.setName(jsonObject.getString("name"));
                        this.locationDao.saveLocation(loc);
                        architectView.callJavascript("World.addGeoMarker(" + loc.getLatitude() + "," + loc.getLongitude() + "," + loc.getAltitude() + ", '" + loc.getName() + "')");
                        architectView.callJavascript("alert('marker was added')");
                        Log.d("save complete", "Location has been saved");
                    } else if (jsonObject.getString("type").equals("clickLocation")) {
                        Location loc = new Location();
                        loc.setAltitude(jsonObject.getDouble("altitude"));
                        loc.setLatitude(jsonObject.getDouble("latitude"));
                        loc.setLongitude(jsonObject.getDouble("longitude"));
                        loc.setName(jsonObject.getString("name"));
                        handleClickFromAR(loc);
                    }
                } catch (Exception e) {
                    Log.d("jsonError", "could not extract location from json");
                }

            });
        } catch (IOException e) {
            Toast.makeText(this, getString(R.string.error_loading_ar_experience), Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Exception while loading arExperience " + arExperience + ".", e);
        }
    }


    @Override
    @SuppressLint("MissingPermission")
    public void onResume() {
        super.onResume();
        architectView.onResume(); // Mandatory ArchitectView lifecycle call
        //mFusedLocationClient.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper());
    }

    @Override
    protected void onPause() {
        super.onPause();
        architectView.onPause(); // Mandatory ArchitectView lifecycle call
        //mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        /*
         * Deletes all cached files of this instance of the ArchitectView.
         * This guarantees that internal storage for this instance of the ArchitectView
         * is cleaned and app-memory does not grow each session.
         *
         * This should be called before architectView.onDestroy
         */
        architectView.clearCache();
        architectView.onDestroy(); // Mandatory ArchitectView lifecycle call
    }

    @Override
    //back navigation to last activity
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void addLocationsToAR(List<Location> allMarkers) {
        if (allMarkers != null && allMarkers.size() > 0) {
            for (Location loc : allMarkers) {
                String jsString = "World.addGeoMarker(" + loc.getLatitude() + "," + loc.getLongitude() + "," + loc.getAltitude() + ",'" + loc.getName() + "')";
                architectView.callJavascript(jsString);
            }
        } else {
            Log.d("No Locations", "Locations not found");
        }
    }

    private void handleClickFromAR(Location location) {
        try {
            if (checkIfSessionExists()) {
                String locationParameter = String.format("x=%s&y=%s&z=%s&locationName=%s", location.getLongitude(), location.getLatitude(), location.getAltitude(), location.getName());
                String urlParams = String.format("?matchID=%s&playerName=%s&%s", sessionId, userID, locationParameter);
                String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.ADD_POINTS, urlParams);
                String message = new AsyncRestCallServer().execute(url).get();

                // JavaScript is calling this Method in separate Threat
                runOnUiThread(() -> {
                    ArActivity.this.toastMessage(message);
                    locations.remove(location);
                    saveLocationsInSharedPreferences(locations);
                    ArActivity.this.onBackPressed();
                    ArActivity.this.finish();
                });

            } else {
                Log.d(ArActivity.TAG, "Session not found, can´t add points");
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }


    private void saveLocationsInSharedPreferences(List<Location> locations){
        SharedPreferences sharedPreferences = getSharedPreferences("LocationContainer" ,PRIVATE_MODE_FOR_LOCAL_DB);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        for (String loc : LocationFactory.createStringLocationListFromLocations(locations)) {
            String key = loc;
            editor.putString(key, loc);
        }
        editor.apply();
    }

    private boolean checkIfSessionExists() {
        return sessionId != null && !sessionId.isEmpty();
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
