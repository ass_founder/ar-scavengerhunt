package de.htwg.moco.arscavengerhunt.model.factories;



import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.impl.Location;

/**
 * This class is used to create Locations as locations or as a String
 */
public final class LocationFactory {
    /**
     * This method converts a list of strings to a list of locations. The strings need to represent a location object
     * @param locationsAsStrings the list of String locations
     * @return a list of location objects. One for each string
     */
    public static List<Location> createLocationListFromStringList(List<String> locationsAsStrings){

        List<Location> locations = new ArrayList<>();
        for (String locationsAsString : locationsAsStrings) {
            Gson gson = new Gson();
            Location location = gson.fromJson(locationsAsString, Location.class);
            locations.add(location);
        }

        return locations;
    }

    /**
     * This method creates a List of Strings from a List of locations. This Strings are a json representation of the location
     * @param locations the list of locations that should be converted
     * @return the List of JSON strings
     */
    public static List<String> createStringLocationListFromLocations(List<Location> locations){
        List<String> locationsAsStrings = new ArrayList<>();

        for (Location location : locations) {
            locationsAsStrings.add(location.toJsonString());
        }
        return locationsAsStrings;
    }
}
