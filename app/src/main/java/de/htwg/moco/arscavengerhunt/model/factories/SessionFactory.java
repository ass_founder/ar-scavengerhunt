package de.htwg.moco.arscavengerhunt.model.factories;

import com.google.firebase.database.DataSnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.htwg.moco.arscavengerhunt.model.impl.DefaultHuntImpl;
import de.htwg.moco.arscavengerhunt.model.impl.DefaultSessionImpl;
import de.htwg.moco.arscavengerhunt.model.impl.Location;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * This class creates Session objects
 */
public final class SessionFactory {

    /**
     * This method converts a DataSnapshot from firebase to a Session object of the type 'Default Impl' which is the implementation that represents our firebase session object
     * @param snapshot the snapshot from firebase
     * @return a new Session Object
     */
    public static Session createSessionFromDataSnapshot(DataSnapshot snapshot){
       DefaultSessionImpl defaultMatch = new DefaultSessionImpl();
        //convert the part where firebase messes the uuid up to something usefull
        Object obj = snapshot.child("matchID").getValue();
        HashMap<String, Long> matchID = (HashMap<String, Long>) obj;
        Long mostBits = matchID.get("mostSignificantBits");
        Long leastBits = matchID.get("leastSignificantBits");
        defaultMatch.setMatchID(new UUID(mostBits, leastBits));
        String name = (String) snapshot.child("name").getValue();
        defaultMatch.setName(name);
        //look if the match started
        boolean started = (boolean) snapshot.child("started").getValue();
        if(started){
            defaultMatch.startGame();
        }
        Map<String, Long> playerPoints = (Map<String, Long>) snapshot.child("playerPoints").getValue();
        defaultMatch.setPlayerPoints(playerPoints);

        Long winingPoints = (Long) snapshot.child("winingPoints").getValue();

        defaultMatch.setWiningPoints(winingPoints.intValue());
        Hunt hunt = snapshot.child("hunt").getValue(DefaultHuntImpl.class);
        defaultMatch.setHunt(hunt);
        return defaultMatch;
    }

    /**
     * This method converts a JSON representation of a Session Object to a Session Object. It returns a default impl session object.
     * @param jsonObject the JSON represenation of a Session
     * @return the Session object
     * @throws JSONException if it can not convert the String. For example the String represents not a Session object
     */
    public static Session createSessionFromJsonData(JSONObject jsonObject) throws JSONException {
        DefaultSessionImpl defaultMatch = new DefaultSessionImpl();
        String idAsString = (String) jsonObject.get("matchID");
        defaultMatch.setMatchID(UUID.fromString(idAsString));
        defaultMatch.setName(jsonObject.getString("name"));
        defaultMatch.setStarted(jsonObject.getBoolean("started"));
        defaultMatch.setWiningPoints(jsonObject.getInt("winingPoints"));

        Map<String,Long> playerPoints = new LinkedHashMap<>();

        JSONObject playerObject = (JSONObject) jsonObject.get("playerPoints");
        Iterator<String> iterator = playerObject.keys();

        while (iterator.hasNext()){
            String key = iterator.next();
            Long points = playerObject.getLong(key);
            playerPoints.put(key, points);
        }
        DefaultHuntImpl defaultHunt = new DefaultHuntImpl();
        JSONObject huntObject = (JSONObject) jsonObject.get("hunt");
        defaultHunt.setName(huntObject.getString("name"));
        defaultHunt.setAuthor(huntObject.getString("author"));

        List<Location> locations = new ArrayList<>();

        JSONArray jsonLocations = huntObject.getJSONArray("locations");
        for (int i = 0; i < jsonLocations.length(); i++){
            JSONObject locationObject = (JSONObject) jsonLocations.get(i);
            Location location = new Location();
            location.setName(locationObject.getString("name"));
            location.setAltitude(locationObject.getDouble("altitude"));
            location.setLongitude(locationObject.getDouble("longitude"));
            location.setLatitude(locationObject.getDouble("latitude"));
            locations.add(location);
        }
        defaultHunt.setLocations(locations);
        defaultMatch.setHunt(defaultHunt);
        defaultMatch.setPlayerPoints(playerPoints);
        return defaultMatch;
    }

}
