package de.htwg.moco.arscavengerhunt.util.recyclerview;

import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Model;

public abstract class AbstractDataProvider<T> {

    public static abstract class Data {
        public abstract long getId();

        public abstract boolean isSectionHeader();

        public abstract int getViewType();

        public abstract Model getModel();

        public abstract String getText();

        public abstract boolean isSelected();

        public abstract void setSelected(boolean selected);

        public abstract void setPinned(boolean pinned);

        public abstract boolean isPinned();
    }

    public abstract List<Data> getAllItems();

    public abstract List<T> getAllModels();

    public abstract int getCount();

    public abstract Data getItem(int index);

    public abstract int addItem(Model model);

    public abstract void removeItem(int position);

    public abstract void moveItem(int fromPosition, int toPosition);

    public abstract void swapItem(int fromPosition, int toPosition);

    public abstract int undoLastRemoval();
}
