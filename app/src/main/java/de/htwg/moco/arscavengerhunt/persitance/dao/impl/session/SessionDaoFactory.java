package de.htwg.moco.arscavengerhunt.persitance.dao.impl.session;

public final class SessionDaoFactory {
    public static SessionDao getInstance(String sessionId) {
        return new SessionDao(sessionId);
    }
}
