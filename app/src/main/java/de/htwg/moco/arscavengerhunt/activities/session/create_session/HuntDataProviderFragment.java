package de.htwg.moco.arscavengerhunt.activities.session.create_session;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;


import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;


@SuppressLint("ValidFragment")
public class HuntDataProviderFragment extends Fragment {

    private AbstractDataProvider mDataProvider;

    private List<Hunt> hunts;

    public HuntDataProviderFragment(List<Hunt> hunts){
        this.hunts = hunts;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataProvider = new HuntDataProvider(hunts);

        setRetainInstance(true);  // keep the mDataProvider instance

    }

    public AbstractDataProvider getDataProvider() {
        return mDataProvider;
    }
}
