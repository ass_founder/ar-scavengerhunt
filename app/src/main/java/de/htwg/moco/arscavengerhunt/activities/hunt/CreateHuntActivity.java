package de.htwg.moco.arscavengerhunt.activities.hunt;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import java.util.List;

import de.htwg.moco.arscavengerhunt.BuildConfig;
import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.model.impl.DefaultHuntImpl;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt.UserHuntDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt.UserHuntDaoFactory;
import de.htwg.moco.arscavengerhunt.persitance.database.DatabaseFactory;
import de.htwg.moco.arscavengerhunt.persitance.database.IDatabase;
import de.htwg.moco.arscavengerhunt.util.general.NetworkStateReceiver;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

public class CreateHuntActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String TAG = CreateHuntActivity.class.getSimpleName();
    private static final int ERROR_MSG_DELAY_MILLIS = 3000;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int PRIVATE_MODE_FOR_LOCAL_DB = 0;

    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser firebaseUser;

    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;

    private IDatabase<Hunt> localHuntDatabase;

    private de.htwg.moco.arscavengerhunt.model.impl.Location locationToSave;

    private UserHuntDao userHuntDao;

    private NetworkStateReceiver networkStateReceiver;
    private boolean internetConnection = false;

    private EditText etForLocationName;
    private EditText etForHuntName;
    private TextView errorMsg;
    private Button addLocationButton;
    private Button finishHuntButton;

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_hunt);
        setActionBar();

        firebaseAuth = FirebaseAuth.getInstance();

        mAuthListener = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                Intent i = new Intent(CreateHuntActivity.this, LoginActivity.class);
                //close activity on backButton (new root)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        userHuntDao = UserHuntDaoFactory.getInstance(firebaseUser.getUid());

        localHuntDatabase = DatabaseFactory.getHuntDatabase(getPreferences(PRIVATE_MODE_FOR_LOCAL_DB));

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(new LocationDataProviderFragment(), FRAGMENT_TAG_DATA_PROVIDER)
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.createHunt_recyclerView_container, new DraggableSwipeableLocationFragment(), FRAGMENT_LIST_VIEW)
                    .commit();
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        etForHuntName = findViewById(R.id.et_createHunt_huntName);
        etForLocationName = findViewById(R.id.et_createHunt_locationName);

        errorMsg = findViewById(R.id.tv_createHunt_errorMsg);

        addLocationButton = findViewById(R.id.bt_createHunt_addLocation);
        finishHuntButton = findViewById(R.id.bt_createHunt_finishHunt);

        addLocationButton.setOnClickListener(save -> {
            if (etForLocationName.getText().toString().isEmpty()) {
                setErrorMsgWithDelay(getString(R.string.createHunt_error_noLocationName));
            } else {
                getLastLocation();
                locationToSave.setName(etForLocationName.getText().toString());
                final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
                ((DraggableSwipeableLocationFragment) fragment).notifyItemChanged(getDataProvider().addItem(locationToSave));
                etForLocationName.setText(getString(R.string.emptyString));
            }

        });

        finishHuntButton.setOnClickListener(finish -> {
            if (etForHuntName.getText().toString().isEmpty()) {
                setErrorMsgWithDelay(getString(R.string.createHunt_error_noHuntName));
            } else {
                if(getDataProvider().getAllItems().isEmpty()){
                    setErrorMsgWithDelay(getString(R.string.createHunt_error_noLocationsInList));
                }else{
                    Hunt currentHunt = new DefaultHuntImpl();
                    currentHunt.setAuthor(firebaseUser.getUid());
                    currentHunt.setName(etForHuntName.getText().toString());

                    List<de.htwg.moco.arscavengerhunt.model.impl.Location> allModels = getDataProvider().getAllModels();
                    currentHunt.setLocations(allModels);

                    if (internetConnection) {
                        userHuntDao.saveHunt(currentHunt);
                        toastMessage(getString(R.string.createHunt_success_saveHuntOnline));
                    } else {
                        localHuntDatabase.saveData(currentHunt);
                        toastMessage(getString(R.string.createHunt_success_saveHuntLocal));
                    }

                    onBackPressed();
                }
            }
        });

    }

    @Override
    public void networkAvailable() {
        internetConnection = true;
        if (!localHuntDatabase.loadAllData().isEmpty()) {
            List<Hunt> hunts = localHuntDatabase.loadAllData();
            if (userHuntDao != null) {
                for (Hunt hunt : hunts) {
                    userHuntDao.saveHunt(hunt);
                    localHuntDatabase.deleteData(hunt);
                }
                toastMessage(getString(R.string.createHunt_success_saveHuntSync));
            }
        }
    }

    @Override
    public void networkUnavailable() {
        internetConnection = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }


    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        mLastLocation = task.getResult();

                        locationToSave = new de.htwg.moco.arscavengerhunt.model.impl.Location();

                        locationToSave.setLatitude(mLastLocation.getLatitude());
                        locationToSave.setLongitude(mLastLocation.getLongitude());
                        locationToSave.setAltitude(mLastLocation.getAltitude());


                    } else {
                        Log.w(TAG, "getLastLocation:exception", task.getException());
                        showSnackbar(getString(R.string.no_location_detected));
                    }
                });
    }


    /**
     * This method will be called when a list item is removed
     */
    public void onItemRemoved() {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.createHuntContainer),
                R.string.snack_bar_text_item_removed,
                Snackbar.LENGTH_LONG);

        snackbar.setAction(R.string.snack_bar_action_undo, v -> onItemUndoActionClicked());
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.snackbar_action_color_done));
        snackbar.show();
    }

    private void onItemUndoActionClicked() {
        int position = getDataProvider().undoLastRemoval();
        if (position >= 0) {
            final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
            ((DraggableSwipeableLocationFragment) fragment).notifyItemInserted(position);
        }
    }

    public AbstractDataProvider getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((LocationDataProviderFragment) fragment).getDataProvider();
    }

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void showSnackbar(final String text) {
        View container = findViewById(R.id.location_activity_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(CreateHuntActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    view -> {
                        // Request permission
                        startLocationPermissionRequest();
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        view -> {
                            // Build intent that displays the App settings screen.
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        });
            }
        }
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void setErrorMsgWithDelay(String message) {
        errorMsg.setText(message);
        Handler h = new Handler();
        h.postDelayed(() -> errorMsg.setText(R.string.emptyString), ERROR_MSG_DELAY_MILLIS);
    }

}
