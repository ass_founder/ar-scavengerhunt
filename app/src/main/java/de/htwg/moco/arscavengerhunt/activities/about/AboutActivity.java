package de.htwg.moco.arscavengerhunt.activities.about;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import de.htwg.moco.arscavengerhunt.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        setActionBar();
    }

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
