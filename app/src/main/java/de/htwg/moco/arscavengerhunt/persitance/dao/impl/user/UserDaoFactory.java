package de.htwg.moco.arscavengerhunt.persitance.dao.impl.user;

public final class UserDaoFactory {
    public static UserDao getInstance() {
        return new UserDao();
    }
}
