package de.htwg.moco.arscavengerhunt.activities.profile;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt.UserHuntDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt.UserHuntDaoFactory;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

public class ProfileActivity extends AppCompatActivity {

    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private UserHuntDao userHuntDao;

    private Button logoutButton;


    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setActionBar();

        firebaseAuth = FirebaseAuth.getInstance();

        mAuthListener = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        userHuntDao = UserHuntDaoFactory.getInstance(firebaseUser.getUid());

        userHuntDao.setUpdateEventHandler(innerHuntDao -> {
            List<Hunt> fireBaseHunts = innerHuntDao.getHunts();
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .add(new UserHuntsDataProviderFragment(fireBaseHunts), FRAGMENT_TAG_DATA_PROVIDER)
                        .commitAllowingStateLoss();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.profile_recyclerView_container, new SwipeableUserHuntsFragment(), FRAGMENT_LIST_VIEW)
                        .commitAllowingStateLoss();
            }
        });


        logoutButton = findViewById(R.id.bt_profile_logout);

        logoutButton.setOnClickListener(logout -> {
            firebaseAuth.signOut();
            Toast.makeText(ProfileActivity.this, getString(R.string.profile_logout_message), Toast.LENGTH_LONG).show();
        });


    }

    /**
     * This method will be called when a list item is removed
     */
    public void onItemRemoved(int position) {
        Snackbar snackbar = Snackbar.make(
                findViewById(R.id.profileContainer),
                R.string.snack_bar_text_item_removed,
                Snackbar.LENGTH_LONG);

        snackbar.setAction(R.string.snack_bar_action_undo, v -> onItemUndoActionClicked());
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.snackbar_action_color_done));
        snackbar.show();
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                if(event == DISMISS_EVENT_TIMEOUT){
                    getDataProvider().undoLastRemoval();
                    if (userHuntDao != null) {
                        userHuntDao.deleteHunt((Hunt) getDataProvider().getItem(position).getModel());
                        getDataProvider().removeItem(position);
                        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
                        ((SwipeableUserHuntsFragment) fragment).notifyItemRemoved(position);
                    }
                }
                if (event == DISMISS_EVENT_CONSECUTIVE) {
                    List<AbstractDataProvider.Data> dataSnapShotForFireBaseRemove = ((UserHuntDataProvider)getDataProvider()).getInitialDataSnapShotForFirebase();
                    if (userHuntDao != null) {
                        userHuntDao.deleteHunt((Hunt) dataSnapShotForFireBaseRemove.get(position).getModel());
                    }
                }
            }
        });
    }

    private void onItemUndoActionClicked() {
        int position = getDataProvider().undoLastRemoval();
        if (position >= 0) {
            final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);
            ((SwipeableUserHuntsFragment) fragment).notifyItemInserted(position);
        }
    }

    public AbstractDataProvider getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((UserHuntsDataProviderFragment) fragment).getDataProvider();
    }


    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

}
