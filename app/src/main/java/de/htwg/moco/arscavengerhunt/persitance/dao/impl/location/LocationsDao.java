package de.htwg.moco.arscavengerhunt.persitance.dao.impl.location;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.persitance.dao.AbstractDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.ILocationsDao;
import de.htwg.moco.arscavengerhunt.model.impl.Location;

public class LocationsDao extends AbstractDao<LocationsDao> implements ILocationsDao {

    private static final String TAG = LocationsDao.class.getSimpleName();

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference(Location.DB_NAME);
    private List<Location> locations;


    LocationsDao() {
        LocationsDao dao = this;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                locations = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    locations.add(d.getValue(Location.class));
                }
                notifyAboutUpdate(dao);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "DataBaseError read all Location");
            }
        });
    }

    /**
     * Get all Locations from FireBase
     *
     * @Return List<Location>
     */
    public List<Location> getLocations() {
        return locations;
    }

    public void saveLocation(Location location){
        if(location != null){
            myRef.push().setValue(location);
        }else{
            Log.d(TAG, "Can not save Location with null value.");
        }
    }
}
