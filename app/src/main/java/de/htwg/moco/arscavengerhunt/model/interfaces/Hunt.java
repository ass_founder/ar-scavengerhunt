package de.htwg.moco.arscavengerhunt.model.interfaces;

import java.util.List;

import de.htwg.moco.arscavengerhunt.model.impl.Location;


public interface Hunt extends Model{

    List<Location> getLocations();

    String getName();

    void setName(String name);

    void setAuthor(String author);

    String getAuthor();

    void setLocations(List<Location> locations);

    void addLocation(Location location);

    void removeLocation(Location location);

}
