package de.htwg.moco.arscavengerhunt.util.serverConnection;

public final class ServerConstants {

    public static final String serverURL = "http://v22018015549559138.ultrasrv.de:8080";

    public static final String socketURL = "v22018015549559138.ultrasrv.de";

    public static final int socketPort = 7080;

    public static final String sessionPreFix = "sessions";

    public static final String createSession = "createSession";

    public static final String deleteSession = "deleteSession";

    public static final String availableSessions = "getAvailableSessions";

    public static final String joinSession = "joinSession";

    public static final String leaveSession = "leaveSession";

    public static final String ADD_POINTS = "addPoints";

    public static final String startGame = "startGame";
}
