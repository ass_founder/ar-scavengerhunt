package de.htwg.moco.arscavengerhunt.activities.hunt;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.impl.Location;
import de.htwg.moco.arscavengerhunt.model.interfaces.Model;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

public class LocationDataProvider extends AbstractDataProvider<Location> {
    private List<ConcreteData> mData;
    private ConcreteData mLastRemovedData;
    private int mLastRemovedPosition = -1;

    public LocationDataProvider() {
        mData = new LinkedList<>();
    }

    @Override
    public List<Data> getAllItems() {
        List<Data> dataList = new LinkedList<>();
        dataList.addAll(mData);
        return dataList;
    }

    @Override
    public List<Location> getAllModels() {
        List<Location> locations = new LinkedList<>();
        for (ConcreteData concreteData : mData) {
            locations.add((Location) concreteData.getModel());
        }
        return locations;
    }

    @Override
    public int getCount() {
        return mData.size();
    }


    @Override
    public int addItem(Model model) {
        Location location = (Location) model;
        mData.add(new ConcreteData(mData.size(), 0, location.getName(), location));

        //for notify recyclerView
        return mData.size();
    }

    @Override
    public Data getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mData.get(index);
    }


    @Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < mData.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mData.size();
            }

            mData.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final ConcreteData item = mData.remove(fromPosition);

        mData.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    @Override
    public void swapItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        Collections.swap(mData, toPosition, fromPosition);
        mLastRemovedPosition = -1;
    }

    @Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final ConcreteData removedItem = mData.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public static final class ConcreteData extends Data {

        private final long mId;
        private final String mText;
        private Location mLocationModel;
        private final int mViewType;
        private boolean mSelected = false;
        private boolean mPinned;

        ConcreteData(long id, int viewType, String text, Location locationModel) {
            mId = id;
            mViewType = viewType;
            mLocationModel = locationModel;
            mText = makeText(id, text);
        }

        private static String makeText(long id, String text) {
            final StringBuilder sb = new StringBuilder();

            //for DEBUG
            //sb.append(id);
            //sb.append(" - ");

            sb.append(text);

            return sb.toString();
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public int getViewType() {
            return mViewType;
        }

        @Override
        public Model getModel() {
            return mLocationModel;
        }

        @Override
        public long getId() {
            return mId;
        }

        @Override
        public String toString() {
            return mText;
        }

        @Override
        public String getText() {
            return mText;
        }

        @Override
        public boolean isSelected() {
            return mSelected;
        }

        @Override
        public void setSelected(boolean selected) {
            mSelected = selected;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }
    }
}
