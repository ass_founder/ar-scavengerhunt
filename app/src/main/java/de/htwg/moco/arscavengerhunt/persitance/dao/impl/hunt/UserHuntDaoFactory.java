package de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt;

import android.support.annotation.NonNull;

public final class UserHuntDaoFactory {
    public static UserHuntDao getInstance(@NonNull String userUuid) {
        return new UserHuntDao(userUuid);
    }
}
