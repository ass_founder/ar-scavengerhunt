package de.htwg.moco.arscavengerhunt.model.impl;


public class User {

    public final static String DB_NAME = "users";

    private String uuid;
    private String name;

    public User() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
