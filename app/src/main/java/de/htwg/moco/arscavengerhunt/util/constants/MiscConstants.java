package de.htwg.moco.arscavengerhunt.util.constants;

/**
 * This contains constants for various things.
 */
public final class MiscConstants {
    /**
     * The key to get the Session ID in an intent
     */
    public static final String KEY_SESSION_ID = "sessionId";

    /**
     * The key to get the User ID in an intent
     */
    public static final String KEY_USER_ID = "userId";
}
