package de.htwg.moco.arscavengerhunt.util.wikitude;

import de.htwg.moco.arscavengerhunt.activities.ar.ArActivity;

import static com.wikitude.common.camera.CameraSettings.*;

public class DefaultConfigs {

    private DefaultConfigs() {}

    // Defaults configuration for samples
    public static final Class DEFAULT_ACTIVITY = ArActivity.class;
    public static final CameraPosition DEFAULT_CAMERA_POSITION = CameraPosition.DEFAULT;
    public static final CameraResolution DEFAULT_CAMERA_RESOLUTION = CameraResolution.HD_1280x720;
    public static final CameraFocusMode DEFAULT_CAMERA_FOCUS_MODE = CameraFocusMode.CONTINUOUS;
    public static final boolean DEFAULT_CAMERA_2_ENABLED = false;
    public static final int DEFAULT_AR_FEATURES = 15;

}
