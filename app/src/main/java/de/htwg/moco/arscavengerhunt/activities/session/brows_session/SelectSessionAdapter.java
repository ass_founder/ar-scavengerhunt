package de.htwg.moco.arscavengerhunt.activities.session.brows_session;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

public class SelectSessionAdapter extends RecyclerView.Adapter<SelectSessionAdapter.MyViewHolder>{

    private static final String TAG = SelectSessionAdapter.class.getSimpleName();

    private SelectSessionAdapter.EventListener mEventListener;
    private AbstractDataProvider mProvider;
    private View.OnClickListener mItemViewOnClickListener;

    public interface EventListener {
        void onItemViewClicked(View v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public FrameLayout mContainer;
        public TextView mTextView;

        public MyViewHolder(View v) {
            super(v);
            mContainer = v.findViewById(R.id.container);
            mTextView = v.findViewById(android.R.id.text1);
        }

    }

    public SelectSessionAdapter(AbstractDataProvider dataProvider) {
        mProvider = dataProvider;
        mItemViewOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemViewClick(v);
            }
        };

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return mProvider.getItem(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return mProvider.getItem(position).getViewType();
    }

    @Override
    public SelectSessionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item, parent, false);
        return new SelectSessionAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SelectSessionAdapter.MyViewHolder holder, int position) {
        final AbstractDataProvider.Data item = mProvider.getItem(position);

        // set onClickListener
        holder.mContainer.setOnClickListener(mItemViewOnClickListener);

        // set text
        holder.mTextView.setText(item.getText());


        if(item.isSelected()){
            holder.mTextView.setBackgroundResource(R.drawable.bg_item_onclick_selected);
        }else{
            holder.mTextView.setBackgroundResource(R.drawable.bg_swipe_item_neutral);
        }
    }

    @Override
    public int getItemCount() {
        return mProvider.getCount();
    }

    private void onItemViewClick(View v) {
        if (mEventListener != null) {
            mEventListener.onItemViewClicked(RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
        }
    }

    public SelectSessionAdapter.EventListener getEventListener() {
        return mEventListener;
    }

    public void setEventListener(SelectSessionAdapter.EventListener eventListener) {
        mEventListener = eventListener;
    }
}
