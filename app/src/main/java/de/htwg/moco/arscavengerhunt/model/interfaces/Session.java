package de.htwg.moco.arscavengerhunt.model.interfaces;


import java.util.Set;
import java.util.UUID;

import de.htwg.moco.arscavengerhunt.model.impl.Location;

public interface Session extends Model {

    String getName();

    UUID getMatchID();

    Hunt getHunt();

    Set<String> getPlayers();

    void setMatchID(UUID matchID);

    long getPlayerPointsByUserId(String uid);

    void addPlayer(String player);

    boolean addPoints(String player, int points);

    String endGame();

    boolean hasLocation(Location location);

    void startGame();

    boolean isStarted();

}
