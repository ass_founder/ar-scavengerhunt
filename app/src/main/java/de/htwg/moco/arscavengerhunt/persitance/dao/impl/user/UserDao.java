package de.htwg.moco.arscavengerhunt.persitance.dao.impl.user;

import android.util.Log;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.persitance.dao.AbstractDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.IUserDao;
import de.htwg.moco.arscavengerhunt.model.impl.User;


public class UserDao extends AbstractDao<UserDao> implements IUserDao {

    private static final String TAG = UserDao.class.getSimpleName();

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference(User.DB_NAME);

    private List<User> userList;


    UserDao() {
        UserDao dao = this;
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userList = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    userList.add(ds.getValue(User.class));
                }
                notifyAboutUpdate(dao);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "DataBaseError read all Users");
            }
        });
    }


    /***
     * Get one specific User or null by uid
     * @Return Useer
     * */
    public User getUser(String uid) {
        for (User u : getUsers()) {
            if (u.getUuid().equals(uid)) {
                return u;
            }
        }
        return null;
    }

    /**
     * Get all Users from FireBase
     *
     * @Return List<Useer>
     */
    public List<User> getUsers() {
        return userList;
    }


    /***
     * Saves new Users in FireBase
     * */
    public void saveNewUser(FirebaseUser user) {
        if (user != null) {

            myRef.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        Log.d(TAG, "User already exists.");
                    } else {
                        User userSave = new User();
                        userSave.setUuid(user.getUid());
                        userSave.setName(user.getDisplayName());
                        myRef.child(user.getUid()).setValue(userSave);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            Log.d(TAG, "Can not save User with null value.");
        }

    }
}
