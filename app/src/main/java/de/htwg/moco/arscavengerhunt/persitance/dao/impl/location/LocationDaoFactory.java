package de.htwg.moco.arscavengerhunt.persitance.dao.impl.location;

public final class LocationDaoFactory {
    public static LocationsDao getInstance() {
        return new LocationsDao();
    }
}
