package de.htwg.moco.arscavengerhunt.activities.game;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.ar.ArActivity;
import de.htwg.moco.arscavengerhunt.activities.dashboard.DashboardActivity;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.activities.map_and_location.MapsActivity;
import de.htwg.moco.arscavengerhunt.model.factories.LocationFactory;
import de.htwg.moco.arscavengerhunt.model.impl.Location;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.session.SessionDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.session.SessionDaoFactory;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.user.UserDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.user.UserDaoFactory;
import de.htwg.moco.arscavengerhunt.util.constants.MiscConstants;
import de.htwg.moco.arscavengerhunt.util.serverConnection.AsyncRestCallServer;
import de.htwg.moco.arscavengerhunt.util.serverConnection.ServerConstants;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleCategory;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleData;
import de.htwg.moco.arscavengerhunt.util.wikitude.SampleJsonParser;

public class GameActivity extends AppCompatActivity {

    private static final String TAG = GameActivity.class.getSimpleName();
    private List<SampleCategory> categories;
    private static final String sampleDefinitionsPath = "samples/samples.json";
    private static final int PRIVATE_MODE_FOR_LOCAL_DB = 0;
    private String sessionId;
    private boolean doubleBackToExitPressedOnce = false;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private SessionDao sessionDao;
    private Session session;

    private Button arViewButton;
    private Button mapButton;
    private Button surrenderButton;

    private TextView currentUserPoints;
    private TextView rivalPoints;

    private List<String> gameLocationsAsStrings;


    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);


        firebaseAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                Intent i = new Intent(GameActivity.this, LoginActivity.class);
                //close activity on backButton (new root)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };


        sessionId = (String) getIntent().getExtras().get(MiscConstants.KEY_SESSION_ID);
        firebaseUser = firebaseAuth.getCurrentUser();

        sessionDao = SessionDaoFactory.getInstance(sessionId);


        currentUserPoints = findViewById(R.id.tv_game_userPoints);
        rivalPoints = findViewById(R.id.tv_game_rivalPoints);

        sessionDao.setUpdateEventHandler(innerSessionDao -> {
            session = innerSessionDao.getSession();

            if (session == null) {
                Intent i = new Intent(GameActivity.this, DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                toastMessage(getString(R.string.game_gameOver_text));
                finish();
            } else {
                readLocations();

                StringBuilder rivalPointsBuilder = new StringBuilder();
                Set<String> players = session.getPlayers();
                int pointsWin = session.getHunt().getLocations().size();
                for (String s : players) {
                    if (s.equals(firebaseUser.getUid())) {
                        long pointsOfPlayer = session.getPlayerPointsByUserId(s);
                        if(pointsOfPlayer >= pointsWin){
                            toastMessage(getString(R.string.game_gameOver_youWonTheGame));
                            leaveMatch();
                            killAll();
                        }else{
                            currentUserPoints.setText(String.valueOf(pointsOfPlayer));
                        }
                    } else {
                        long rivalPoints = session.getPlayerPointsByUserId(s);
                        if(rivalPoints >= pointsWin){
                            UserDao userDao = UserDaoFactory.getInstance();
                            userDao.setUpdateEventHandler(innerUserDao->{
                                String userName = innerUserDao.getUser(s).getName();
                                toastMessage(userName+getString(R.string.game_gameOver_rivalWonTheGame));
                                leaveMatch();
                                killAll();
                            });
                        }else{
                            rivalPointsBuilder.append(String.valueOf(rivalPoints) + " ");
                        }
                    }
                }
                if(players.size() <= 1){
                    toastMessage(getString(R.string.game_gameOver_rivalsLeft));
                    deleteMatch();
                    killAll();
                }

                rivalPoints.setText(rivalPointsBuilder.toString().trim());
            }

        });


        mapButton = findViewById(R.id.bt_game_mapView);
        mapButton.setOnClickListener(mapView -> {
            Intent i = new Intent(GameActivity.this, MapsActivity.class);
            i.putStringArrayListExtra("LocationList", (ArrayList<String>) gameLocationsAsStrings);
            startActivity(i);
        });

        arViewButton = findViewById(R.id.bt_game_arView);

        final String json = SampleJsonParser.loadStringFromAssets(this, sampleDefinitionsPath);
        categories = SampleJsonParser.getCategoriesFromJsonString(json);

        arViewButton.setOnClickListener(arView -> {
            final SampleData sampleData = categories.get(0).getSamples().get(0);
            final Intent intent = new Intent(GameActivity.this, sampleData.getActivityClass());
            intent.putExtra(ArActivity.INTENT_EXTRAS_KEY_SAMPLE, sampleData);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(MiscConstants.KEY_SESSION_ID, sessionId);
            intent.putStringArrayListExtra("LocationList", (ArrayList<String>) gameLocationsAsStrings);
            intent.putExtra(MiscConstants.KEY_USER_ID, firebaseUser.getUid());
            startActivity(intent);
        });


        surrenderButton = findViewById(R.id.bt_game_surrender);
        surrenderButton.setOnClickListener(surrenderGame -> {
            leaveMatch();
            deleteMatch();
            killAll();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (session != null) {
            readLocations();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            leaveMatch();
            killAll();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.game_back_warning_text), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    public List<String> loadLocationsFormSharedPreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("LocationContainer", 0);
        List<String> locationsAsString = new ArrayList<>();
        Map<String, ?> all = sharedPreferences.getAll();

        for (String key : all.keySet()) {
            locationsAsString.add((String) all.get(key));
        }


        return locationsAsString;
    }

    private void killAll(){
        sessionDao.removeSessionDaoListener();
        sessionDao.removeUpdateEventHandler();
        finish();
        return;
    }

    private void readLocations() {
        if (gameLocationsAsStrings == null) {
            gameLocationsAsStrings = new ArrayList<>();
            for (Location location : session.getHunt().getLocations()) {
                gameLocationsAsStrings.add(location.toJsonString());
            }
            saveLocationsInSharedPreferences(session.getHunt().getLocations());
        } else {
            //toDo: get locations form shared preferences
            List<String> locations = loadLocationsFormSharedPreferences();
            if (locations != null) {
                gameLocationsAsStrings = locations;
            }
        }
    }

    private void leaveMatch(){
        String urlParameter = String.format("?matchID=%s&playerName=%s", sessionId, firebaseUser.getUid());
        String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.leaveSession, urlParameter);
        try {
            new AsyncRestCallServer().execute(url).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, e.getMessage());
        }
    }


    private void deleteMatch(){
        String urlParameter = String.format("?sessionId=%s", sessionId);
        String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.deleteSession, urlParameter);
        try {
            new AsyncRestCallServer().execute(url).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void saveLocationsInSharedPreferences(List<Location> locations) {
        SharedPreferences sharedPreferences = getSharedPreferences("LocationContainer", PRIVATE_MODE_FOR_LOCAL_DB);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        for (String loc : LocationFactory.createStringLocationListFromLocations(locations)) {
            String key = loc;
            editor.putString(key, loc);
        }
        editor.apply();
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
