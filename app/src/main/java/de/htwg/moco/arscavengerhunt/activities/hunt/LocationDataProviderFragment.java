package de.htwg.moco.arscavengerhunt.activities.hunt;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;


public class LocationDataProviderFragment extends Fragment {
    private AbstractDataProvider mDataProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);  // keep the mDataProvider instance
        mDataProvider = new LocationDataProvider();
    }

    public AbstractDataProvider getDataProvider() {
        return mDataProvider;
    }
}
