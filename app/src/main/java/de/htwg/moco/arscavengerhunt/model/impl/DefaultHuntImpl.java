package de.htwg.moco.arscavengerhunt.model.impl;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;

public class DefaultHuntImpl implements Hunt {

    public static final String DB_NAME = "hunts";

    @SerializedName("locations")
    private List<Location> locations = new ArrayList<>();

    @SerializedName("name")
    private String name ="";

    @SerializedName("author")
    private String author ="";

    public DefaultHuntImpl(){

    }


    @Override
    public List<Location> getLocations() {
        return locations;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    @Override
    public void addLocation(Location location) {
        this.locations.add(location);
    }

    @Override
    public void removeLocation(Location location) {
        this.locations.remove(location);
    }


    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (Location location : locations) {
                builder.append(location.toString());
        }
        return String.format("name:%s author: %s locations:%s", name, author, builder.toString());
    }


}
