package de.htwg.moco.arscavengerhunt.activities.session.brows_session;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

@SuppressLint("ValidFragment")
public class SessionDataProviderFragment extends Fragment {

    private AbstractDataProvider mDataProvider;

    private List<Session> sessions;

    public SessionDataProviderFragment(List<Session> sessions) {
        this.sessions = sessions;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataProvider = new SessionDataProvider(sessions);

        setRetainInstance(true);  // keep the mDataProvider instance

    }

    public AbstractDataProvider getDataProvider() {
        return mDataProvider;
    }
}
