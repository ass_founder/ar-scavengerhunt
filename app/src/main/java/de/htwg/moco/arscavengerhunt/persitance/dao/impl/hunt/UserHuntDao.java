package de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.impl.DefaultHuntImpl;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.persitance.dao.AbstractDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.IHuntDao;

public class UserHuntDao extends AbstractDao<UserHuntDao> implements IHuntDao {
    private static final String TAG = UserHuntDao.class.getSimpleName();

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference(DefaultHuntImpl.DB_NAME);
    private List<Hunt> hunts;


    UserHuntDao(String userUuid) {
        UserHuntDao dao = this;
        myRef.orderByChild("author").equalTo(userUuid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hunts = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    hunts.add(d.getValue(DefaultHuntImpl.class));
                }
                notifyAboutUpdate(dao);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "DataBaseError read all Location");
            }
        });
    }

    /**
     * Get all Hunts from FireBase
     *
     * @Return List<Hunt>
     */
    public List<Hunt> getHunts() {
        return hunts;
    }

    public void saveHunt(Hunt hunt) {
        if (hunt != null) {
            String huntKey = this.createHuntKey(hunt);
            myRef.child(huntKey).setValue(hunt);
        } else {
            Log.d(TAG, "Can not save Hunt with null value.");
        }
    }

    /**
     * Deletes the hunt from firebase
     * @param hunt the hunt to delete
     */
    public void deleteHunt(Hunt hunt){

        if(hunt != null) {
            String huntKey = this.createHuntKey(hunt);
            myRef.child(huntKey).removeValue();
        }
    }

    public static String createHuntKey(Hunt hunt){
        return String.format("%s-%s", hunt.getAuthor(),hunt.getName());
    }
}
