package de.htwg.moco.arscavengerhunt.util.serverConnection;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;


public class ServerListener extends AbstractServerListener<ServerListener> {
    public static final String TAG = ServerListener.class.getSimpleName();

    private ConnectionToServer server;
    private Socket clientSocket;

    private boolean refresh = false;

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            SocketAddress address = new InetSocketAddress(ServerConstants.socketURL, ServerConstants.socketPort);
            clientSocket = new Socket();
            clientSocket.connect(address);

            server = new ConnectionToServer(clientSocket,this);

        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }

        return refresh;
    }

    public void close() throws IOException {
        server.close();
        clientSocket.close();
    }


    private class ConnectionToServer {
        DataInputStream in;
        PrintStream out;
        Socket socket;

        ConnectionToServer(Socket socket, ServerListener parent) throws IOException {
            this.socket = socket;
            InputStream socketInputStream = socket.getInputStream();
            in = new DataInputStream(socketInputStream);
            Thread read = new Thread(() -> {
                while (true) {
                    try {

                        BufferedReader buff = new BufferedReader(new InputStreamReader(in));
                        String temp;
                        temp = buff.readLine();
                        Log.d(TAG,temp);
                        if (temp != null && temp.startsWith("session")) {
                            ServerListener listener = parent ;
                            parent.notifyAboutUpdate(listener);
                            refresh = true;
                        } else {
                            refresh = false;
                        }

                    } catch (Exception se) {
                        close();
                        break;
                    }
                }
            });

            read.setDaemon(true);
            read.start();
        }

        public void close() {
            try {
                in.close();
                out.close();
                socket.close();
            } catch (Exception e) {

            }

        }
    }
}
