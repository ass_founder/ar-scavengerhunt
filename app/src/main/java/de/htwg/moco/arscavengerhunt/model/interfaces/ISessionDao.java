package de.htwg.moco.arscavengerhunt.model.interfaces;


public interface ISessionDao {
    Session getSession();
    void removeSessionDaoListener();
}
