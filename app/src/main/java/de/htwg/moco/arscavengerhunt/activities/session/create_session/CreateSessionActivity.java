package de.htwg.moco.arscavengerhunt.activities.session.create_session;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import de.htwg.moco.arscavengerhunt.R;
import de.htwg.moco.arscavengerhunt.activities.lobby.LobbyActivity;
import de.htwg.moco.arscavengerhunt.activities.login.LoginActivity;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt.UserHuntDao;
import de.htwg.moco.arscavengerhunt.persitance.dao.impl.hunt.UserHuntDaoFactory;
import de.htwg.moco.arscavengerhunt.util.constants.MiscConstants;
import de.htwg.moco.arscavengerhunt.util.serverConnection.AsyncRestCallServer;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;
import de.htwg.moco.arscavengerhunt.util.serverConnection.ServerConstants;

public class CreateSessionActivity extends AppCompatActivity {

    private static final String TAG = CreateSessionActivity.class.getSimpleName();

    public static final int ERROR_MSG_DELAY_MILLIS = 3000;
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";
    private static final String FRAGMENT_LIST_VIEW = "list view";

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private EditText etSessionName;
    private Button openSessionButton;
    private TextView errorMsg;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser() == null) {
                Intent i = new Intent(CreateSessionActivity.this, LoginActivity.class);
                //close activity on backButton (new root)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        setContentView(R.layout.activity_create_session);

        UserHuntDao userHuntDao = UserHuntDaoFactory.getInstance(firebaseUser.getUid());

        userHuntDao.setUpdateEventHandler((innerHuntDao) -> {
            List<Hunt> fireBaseHunts = innerHuntDao.getHunts();
            if (savedInstanceState == null) {

                if (getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER) != null) {
                    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER)).commit();
                    getSupportFragmentManager().beginTransaction()
                            .add(new HuntDataProviderFragment(fireBaseHunts), FRAGMENT_TAG_DATA_PROVIDER)
                            .commitAllowingStateLoss();
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .add(new HuntDataProviderFragment(fireBaseHunts), FRAGMENT_TAG_DATA_PROVIDER)
                            .commitAllowingStateLoss();
                }

                if (getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW) != null) {
                    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW)).commit();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.createSession_recyclerView_container, new SelectHuntFragment(), FRAGMENT_LIST_VIEW)
                            .commitAllowingStateLoss();
                    ((SelectHuntFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW)).notifyItemChanged();
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.createSession_recyclerView_container, new SelectHuntFragment(), FRAGMENT_LIST_VIEW)
                            .commitAllowingStateLoss();
                }
            }
        });

        setActionBar();

        etSessionName = findViewById(R.id.et_createSession_name);
        openSessionButton = findViewById(R.id.bt_createSession_openSession);
        errorMsg = findViewById(R.id.tv_createSession_errorMsg);

        openSessionButton.setOnClickListener(openSession -> {
            if (etSessionName.getText().toString().isEmpty()) {
                setErrorMsgWithDelay(getString(R.string.createSession_error_noSessionName));
            } else {
                if (!isListElementSelected()) {
                    setErrorMsgWithDelay(getString(R.string.createSession_error_noHuntSelected));
                } else {
                    try {
                        String uid = firebaseAuth.getUid();
                        String sessionName = etSessionName.getText().toString().replaceAll(" ","%20");
                        String huntName = UserHuntDao.createHuntKey(this.getSelectedItem());
                        String urlParameter = String.format("?name=%s&huntName=%s&userName=%s", sessionName, huntName, uid);
                        String url = String.format("%s/%s/%s%s", ServerConstants.serverURL, ServerConstants.sessionPreFix, ServerConstants.createSession, urlParameter);
                        String sessionId = new AsyncRestCallServer().execute(url).get();
                        Intent intent = new Intent(CreateSessionActivity.this, LobbyActivity.class);
                        intent.putExtra(MiscConstants.KEY_SESSION_ID, sessionId.replaceAll("\"",""));
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG,e.getMessage());
                    }

                }
            }
        });
    }

    private Hunt getSelectedItem(){
        for (Object o : getDataProvider().getAllItems()) {
            AbstractDataProvider.Data data = (AbstractDataProvider.Data) o;
            if(data.isSelected()){
                return (Hunt)data.getModel();
            }
        }
        return null;

    }

    public AbstractDataProvider getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
        return ((HuntDataProviderFragment) fragment).getDataProvider();
    }

    /**
     * This method will be called when a list item is clicked
     *
     * @param position The position of the item within data set
     */
    public void onItemClicked(int position) {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST_VIEW);

        // for selecting only one item
        List<AbstractDataProvider.Data> dataList = getDataProvider().getAllItems();
        for (AbstractDataProvider.Data data : dataList) {
            data.setSelected(false);
        }

        AbstractDataProvider.Data data = getDataProvider().getItem(position);
        data.setSelected(true);

        ((SelectHuntFragment) fragment).notifyItemChanged();

        //For DEBUG
        //toastMessage("Item with ID: " + data.getId() + " selected");
    }

    private boolean isListElementSelected() {
        boolean checkSelected = false;
        List<AbstractDataProvider.Data> dataList = getDataProvider().getAllItems();
        for (AbstractDataProvider.Data data : dataList) {
            if (data.isSelected()) {
                checkSelected = true;
            }
        }
        return checkSelected;
    }

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void setErrorMsgWithDelay(String message) {
        errorMsg.setText(message);
        Handler h = new Handler();
        h.postDelayed(() -> errorMsg.setText(R.string.emptyString), ERROR_MSG_DELAY_MILLIS);
    }

}
