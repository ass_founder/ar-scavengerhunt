package de.htwg.moco.arscavengerhunt.persitance.dao.impl.session;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.htwg.moco.arscavengerhunt.model.factories.SessionFactory;
import de.htwg.moco.arscavengerhunt.model.impl.DefaultSessionImpl;
import de.htwg.moco.arscavengerhunt.model.interfaces.ISessionDao;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.persitance.dao.AbstractDao;

public class SessionDao extends AbstractDao<SessionDao> implements ISessionDao {
    private static final String TAG = SessionDao.class.getSimpleName();

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference(DefaultSessionImpl.DB_NAME);

    private Session session;

    private ValueEventListener valueEventListener;

    public SessionDao(String sessionId) {
        SessionDao dao = this;

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                session = null;
                if (dataSnapshot.getValue() != null) {
                    Session sessionFromDataSnapshot = SessionFactory.createSessionFromDataSnapshot(dataSnapshot);
                    if (sessionFromDataSnapshot.getMatchID().toString().equals(sessionId)) {
                        session = sessionFromDataSnapshot;
                    }
                }
                notifyAboutUpdate(dao);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "DataBaseError read all Location");
            }
        };

        myRef.child(sessionId).addValueEventListener(valueEventListener);
    }

    /**
     * Get specific Session
     *
     * @Return Session
     */
    public Session getSession() {
        return session;
    }

    @Override
    public void removeSessionDaoListener() {
        myRef.removeEventListener(valueEventListener);
    }

}
