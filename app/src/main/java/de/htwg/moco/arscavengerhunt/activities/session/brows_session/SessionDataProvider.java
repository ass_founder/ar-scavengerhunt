package de.htwg.moco.arscavengerhunt.activities.session.brows_session;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.htwg.moco.arscavengerhunt.model.interfaces.Model;
import de.htwg.moco.arscavengerhunt.model.interfaces.Session;
import de.htwg.moco.arscavengerhunt.util.recyclerview.AbstractDataProvider;

public class SessionDataProvider extends AbstractDataProvider<Session> {

    private List<SessionDataProvider.ConcreteData> mData;
    private SessionDataProvider.ConcreteData mLastRemovedData;
    private int mLastRemovedPosition = -1;

    public SessionDataProvider(List<Session> sessions) {
        if(sessions == null){
            mData = new LinkedList<>();
        }else{
            mData = new LinkedList<>();
            for(Session session: sessions){
                mData.add(new ConcreteData(mData.size(),0,session.getName(),session));
            }
        }

    }

    @Override
    public List<Data> getAllItems(){
        List<Data> dataList = new LinkedList<>();
        dataList.addAll(mData);
        return dataList;
    }

    @Override
    public List<Session> getAllModels() {
        List<Session> sessions = new LinkedList<>();
        for(SessionDataProvider.ConcreteData concreteData : mData){
            sessions.add((Session) concreteData.getModel());
        }
        return sessions;
    }

    @Override
    public int getCount() {
        return mData.size();
    }


    @Override
    public int addItem(Model model) {
        Session session = (Session) model;
        mData.add(new ConcreteData(mData.size(),0,session.getName(),session));

        //for notify recyclerView
        return mData.size();
    }

    @Override
    public Data getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mData.get(index);
    }


    @Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < mData.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mData.size();
            }

            mData.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final SessionDataProvider.ConcreteData item = mData.remove(fromPosition);

        mData.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    @Override
    public void swapItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        Collections.swap(mData, toPosition, fromPosition);
        mLastRemovedPosition = -1;
    }

    @Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final SessionDataProvider.ConcreteData removedItem = mData.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public static final class ConcreteData extends Data {

        private final long mId;
        private final String mText;
        private Session mSessionModel;
        private final int mViewType;
        private boolean mSelected = false;
        private boolean mPinned;

        ConcreteData(long id, int viewType, String text,Session sessionModel) {
            mId = id;
            mViewType = viewType;
            mSessionModel = sessionModel;
            mText = makeText(id, text);
        }

        private static String makeText(long id, String text) {
            final StringBuilder sb = new StringBuilder();

            //for DEBUG
            //sb.append(id);
            //sb.append(" - ");

            sb.append(text);

            return sb.toString();
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public int getViewType() {
            return mViewType;
        }

        @Override
        public Model getModel() {
            return mSessionModel;
        }

        @Override
        public long getId() {
            return mId;
        }

        @Override
        public String toString() {
            return mText;
        }

        @Override
        public String getText() {
            return mText;
        }

        @Override
        public boolean isSelected() {
            return mSelected;
        }

        @Override
        public void setSelected(boolean selected) {
            mSelected = selected;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }
    }
}
