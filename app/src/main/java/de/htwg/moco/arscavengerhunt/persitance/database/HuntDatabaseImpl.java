package de.htwg.moco.arscavengerhunt.persitance.database;


import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.htwg.moco.arscavengerhunt.model.impl.DefaultHuntImpl;
import de.htwg.moco.arscavengerhunt.model.interfaces.Hunt;

/**
 * This class saves objects of the Hunt interface. It is testet with the DefaultHuntImpl.
 * An instance of this class is created by the factory DatabaseFactory
 */
public class HuntDatabaseImpl implements IDatabase<Hunt>{

    /**
     * The SharedPreferences used to save the values
     */
    private SharedPreferences preferences;

    /**
     * Creates a  new Instance.
     * @param  preferences is needed to save values
     */
    public HuntDatabaseImpl(@NonNull SharedPreferences preferences){
        this.preferences = preferences;
    }

    @Override
    public List<Hunt> loadAllData() {
        List<Hunt> hunts = new ArrayList<>();
        Map<String, ?> preferencesAll = preferences.getAll();
        for (String key: preferencesAll.keySet()){
            Hunt hunt = this.transFormTOHunt((String) preferencesAll.get(key));
            hunts.add(hunt);
        }
        return hunts;
    }

    @Override
    public void saveData(Hunt data) {
        String valueToSave = this.transfromToJSON(data);
        SharedPreferences.Editor editor = preferences.edit();
        String key = this.generateKey(data);
        if(preferences.contains(key)){
            editor.remove(key);
        }
        editor.putString(key, valueToSave);
        editor.commit();

    }

    @Override
    public void deleteData(Hunt data) {
        String key = this.generateKey(data);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Transforms a hunt to a json String. This method is called from the save data method
     * @param hunt the Hunt which needs to be transformed
     * @return the Hunt as a JSON String
     */
    private String transfromToJSON(Hunt hunt){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(hunt);
    }

    /**
     * Transforms a String back to a Hunt. It will cause an exception if it can´t transform the JSON
     * @param json the JSON as String
     * @return the loaded Hunt
     */
    private Hunt transFormTOHunt(String json){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.fromJson(json, DefaultHuntImpl.class);
    }

    /**
     * Generates the key for the hunt. This key will be used to find it again
     * @param hunt the hunt which needs a key
     * @return the key as String
     */
    private String generateKey(Hunt hunt){
        return String.format("%s-%s", hunt.getAuthor(), hunt.getName());
    }
}
